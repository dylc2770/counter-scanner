//defining the purchases array. (this is where items will go once they have been scanned)
// because each product has multiple properties, we will store each product as an object in this array.
var purchases = [];

//defining the total price(starts at 0) (this will carry the price of all of the items)
var totalPrice = 0;

//defining the products as objects (Think of these as items in a store, you don't really define them anywhere else, they already exist)
var mattPinkPaint = ["Matt Pink Paint", "Decorating", 6.99];
var floralWallpaper = ["Floral Wallpaper", "Decorating", 7.99];
var magnoliaGlossPaint = ["Magnolia Gloss Paint", "Decorating", 5.49];
var weedKiller = ["Weed Killer", "Gardening", 2.99];
var pictureFrame = ["Picture Frame", "Decorating", 8.99];
var plugSocket = ["Plug Socket", "Electrics", 6.99];
var doorbell = ["Doorbell", "Electrics", 15.99];
var mattWhitePaint = ["Matt White Paint", "Decorating", 4.99];
var tiles = ["Tiles", "Decorating", 19.99];
var grassSeed = ["Grass Seed", "Gardening", 1.99];
var lawnMower = ["Lawn Mower", "Gardening", 129.99];


//creating the scanner(we give ourselves a function, scan. this scan function needs something to pass through it, otherwise it doesnt scan anything)
// so we give it some temporary name, product. this can be named anything. But i named it product to stay relevant (:
function scan(product) {
  //so while inside of the scan function, you have to remember this scanner only sees one product at a time, just keep that in mind.
  //create variables for the things inside of the product (name, category and price) (if we would grab the first product, mattPink Paint, and type mattPinkPaint[2],
  // that would print out the price, cause it's at index 2 ) so we are making it easy and creating variables for this.
  var name = product[0];
  var category = product[1];
  var price = product[2];

  // if category is gardening, take 10% off of the price
  // if (category == "Gardening") {
  //   price = price - (price* .10);
  // }

  //add the price to the total price
  totalPrice += price;

  //add the product into the purchases array
  purchases.push([name, category, price]);
}

//The scanning of the products

scan(mattPinkPaint);
scan(floralWallpaper);
scan(magnoliaGlossPaint);
scan(weedKiller);
scan(pictureFrame);
scan(plugSocket);
scan(doorbell);
scan(mattWhitePaint);
scan(tiles);
scan(grassSeed);
scan(lawnMower);

// console.log the receipt
// console.log(purchases[0][0],purchases[0][1] );
// console.log(purchases[1][0],purchases[1][1] );
// console.log("--------------");

//console.log the receipt with a for loop (this is so that you don't need to know the exact length of the array and also to save you time typing all of the items out)
var i;
for (i = 0; i< purchases.length;i++) {
  console.log(purchases[i][0],purchases[i][2]);
  if (purchases[i][1] == "Gardening") {
    var price = purchases[i][2];
      //take not of what the discount will be
      discountedPrice = (price* .10);
      //set the discount on the receipt
      price = price - (price* .10);
      //remove the discounted price from the total price
      totalPrice -= discountedPrice;
      console.log( discountedPrice.toFixed(2) + " discount");
  }
}

console.log("Price: " + totalPrice.toFixed(2) );
